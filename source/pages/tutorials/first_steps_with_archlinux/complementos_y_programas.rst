########################
Complementos y programas
########################

.. contents:: Contenido
    :depth: 2
    :local:

Utilidades del sistema
======================

Yay como gestor de paquetes
___________________________

Yay es un asistente de instalación de paquetes para ArchLinux con capacidad para instalar paquetes desde el Repositorio de Usuarios de Arch Linux (AUR).

Podemos instalarlo del siguiente repositorio de AUR:

.. code-block:: bash

    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si

Google Chrome
_____________

.. code-block:: bash

    yay -S google-chrome

Firefox
_______

.. code-block:: bash

    sudo pacman -S firefox firefox-i18n-es-ar pipewire-jack

Flatpak
_______

.. code-block:: bash

    sudo pacman -S flatpak

.. note::

    Para poder utilizar ``flatpak`` debe reiniciar el sistema.

Thunderbird
___________

Cliente de correo electrónico:

.. code-block:: bash

    sudo pacman -S thunderbird thunderbird-i18n-es-ar

Libre Office
____________

Set de ofimática:

.. code-block:: bash

    sudo pacman -S libreoffice-fresh libreoffice-fresh-es hunspell-es_ar hyphen-es hyphen-en

Extenciones para GNOME Shell
____________________________

Lo mas recomendable es ir a Google Chrome e instalar el Plugin de Gnome Shell Extention:

.. code-block:: bash

    yay -S chrome-gnome-shell

Luego instalar las siguientes extenciones:

- `AppIndicator and KStatusNotifierItem Support. <https://extensions.gnome.org/extension/615/appindicator-support/>`_
- `Blur my Shell. <https://extensions.gnome.org/extension/3193/blur-my-shell/>`_
- `Gnome 40 UI Improvements. <https://extensions.gnome.org/extension/4158/gnome-40-ui-improvements/>`_

Tema para aplicaciones heredadas para GNOME
___________________________________________

Aplicaciones gtk3 no tendran el tema por defecto que tienen las aplicaciones gtk4. Con lo cual debe instalar una extención de gnome:

- `Legacy (GTK3) Theme Scheme Auto Switcher. <https://extensions.gnome.org/extension/4998/legacy-gtk3-theme-scheme-auto-switcher/>`_

Luego deberá seguir el instructivo de la `página <https://fostips.com/gtk3-light-dark-fedora-gnome/>`_. En donde se explica que se debe crear la carpeta ``$HOME/.themes`` y en su interior extraer el contenido del tema que se encuentra en el repositorio `adw-gtk3. <https://github.com/lassekongo83/adw-gtk3/releases>`_

Luego por último en retoques, en la pestaña apariencia, puede seleccionar el tema para las aplicaciones heredadas.

PAMAC para GNOME
________________

Este gestor de paquetes y actualizaciones, es muy completo y tiene soporte para Flatpak, Snap y AUR. Así que es muy bueno y recomendable:

.. code-block:: bash

    git clone https://aur.archlinux.org/pamac-all.git
    cd pamac-all
    makepkg -si

Luego de instalar el gestor, es recomendable reiniciar el PC y habilitar la extención de sistema *Pamac Updates Indicator.*

Alacritty
_________

Alacritty es un simple emulador de terminal acelerado por GPU escrito en Rust. Admite desplazamiento hacia atrás, colores de 24 bits, copiar/pegar, hacer clic en URL y combinaciones de teclas personalizadas.

Instalación
~~~~~~~~~~~

Para poder instalarla basta con ejecutar:

.. code-block:: bash

    sudo pacman -S alacritty

Configuración
~~~~~~~~~~~~~

Kitty almacena su configuración en ``$HOME/.config/alacritty/alacritty.yml``. Se pueden ajustar las fuentes, los colores, los cursores y los comportamientos de desplazamiento hacia atrás. Puedes ver un ejemplo de archivo de configuración `en el GitHub de alacritty. <https://github.com/alacritty/alacritty/blob/master/alacritty.yml>`_

ZSH
___

Z shell (o simplemente zsh) es un potente intérprete de comandos para sistemas operativos de tipo Unix. Zsh se diseñó para poder usarse interactivamente. Se le han incorporado muchas de las características principales de otras shells de Unix como bash, ksh, o tcsh y además posee características propias originales.

Instalación
~~~~~~~~~~~

Para poder instalarla basta con ejecutar:

.. code-block:: bash

    sudo pacman -S zsh

ZSH como shell predeterminada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una vez instalado el paquete zsh debemos hacer que sea la nueva shell de nuestro sistema, podemos ver las shells instalas con el comando ``chsh``:

.. code-block:: bash

    chsh -s /bin/zsh

Configuración
~~~~~~~~~~~~~

La primera vez que ejecutemos zsh nos mostrará un asistente para una primera configuracion básica de zsh, donde deberemos de responder a una serie de preguntas para establecer el tamaño del historial, personalizar el número de líneas a guardar, etc.

Si quieres ejecutar el asistente de forma manual puedes ejecutar:

.. code-block:: bash

    autoload -Uz zsh-newuser-install
    zsh-newuser-install -f

Powerlevel10k
~~~~~~~~~~~~~

Te recomiendo que instales la versión desde su `repositorio <https://github.com/romkatv/powerlevel10k/>`_, ya que el paquete comunitario *zsh-theme-powerlevel10k* históricamente se ha estado rompiendo con frecuencia y durante largos períodos de tiempo. No lo uses.

Su instalación es muy simple, introduce el siguiente comando en la terminal:

.. code-block:: bash

    sudo pacman -S powerline-common powerline-fonts awesome-terminal-fonts
    yay -S zsh-theme-powerlevel10k-git ttf-meslo-nerd-font-powerlevel10k

Cuando finalice la instalación vuelve a poner en la terminal;

.. code-block:: bash

    echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc

Disponemos de un wizard para ayudarnos a configurar nuestro prompt, para ello como terminamos la instalación, debemos reiniciar la terminal. Si luego d hacer esto el wizard no aparece, simplemente ejecuta:

.. code-block:: bash

    p10k configure

Resaltado de sintaxis
_____________________

Si quieres disponer de un resaltado de sintaxis para la shell al igual que en Fish pero para Zsh, puedes instalar *zsh-syntax-highlighting* desde los repositorios:

.. code-block:: bash

    sudo pacman -S zsh-syntax-highlighting

Y añadimos la configuración al *.zshrc*:

.. code-block:: bash

    echo 'source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> ~/.zshrc

Sugerir comandos
________________

Busca en los repositorios oficiales el paquete *zsh-autosuggestions* e instálalo:

.. code-block:: bash

    sudo pacman -S zsh-autosuggestions

Y añadimos la configuración al *.zshrc*:

.. code-block:: bash

    echo 'source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh' >> ~/.zshrc

Nerd fonts
__________

Conjunto completo de fuentes e iconos:

.. code-block:: bash

    sudo pacman -S papirus-icon-theme nerd-fonts

Temas para GTK
______________

Algunos temas de iconos y ventanas para GTK:

.. code-block:: bash

    git clone https://github.com/rtlewis88/rtl88-Themes.git --branch=MBC-Complete-Desktop --depth 1
    find rtl88-Themes/ -maxdepth 1 -type d -iname "Material-Black-*" -exec sudo mv {} /usr/share/themes \;
    sudo chown -R root:root /usr/share/themes
    rm -rf rtl88-Themes
    git clone https://github.com/rtlewis88/rtl88-Themes.git --branch=MBC-Icon-SuperPack --depth 1
    find rtl88-Themes/ -maxdepth 1 -type d -iname "*Numix*" -or -iname "*Suru*" -exec sudo mv {} /usr/share/icons \;
    sudo chown -R root:root /usr/share/icons
    rm -rf rtl88-Themes

Psensors
________

Este software permite la monitorización de sensores de temperatura:

.. code-block:: bash

    sudo pacman -S lm_sensors

Escaneamos todos los sensores de la PC:

.. code-block:: bash

    sudo sensors-detect

Por último instalamos la utilidad de Psensors.

.. code-block:: bash

    sudo pacman -S psensor python-psutil

Programas para desarrollo
=========================

VS Code
_______

Visual Studio Code es un editor de código fuente desarrollado por Microsoft para Windows, Linux, macOS y Web. Incluye soporte para la depuración, control integrado de Git, resaltado de sintaxis, finalización inteligente de código, fragmentos y refactorización de código. Se puede instalar de la siguiente manera:

.. code-block:: bash

    sudo pacman -S gnome-keyring libsecret libgnome-keyring --needed
    yay -S visual-studio-code-bin

Complementos para extensiones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para el complemento de bash IDE, es necesario instalar ``shellcheck``:

.. code-block:: bash

    sudo pacman -S shellcheck

Docker y Docker Compose
_______________________

Para instalar ``docker`` y ``docker-compose`` basta con ejecutar:

.. code-block:: bash

    sudo pacman -S docker docker-buildx docker-compose
    sudo systemctl enable docker.service

.. note::

    Luego para poder usar ``docker`` como usuario normal debemos añadir nuestro usuario al grupo de docker:

    .. code-block:: bash

        sudo usermod -aG docker $USER

.. warning::

    Se debe de configurar la rotación de los logs para que no existan problemas de almacenamiento en un futuro:

    .. code-block:: bash

        sudo nano /etc/docker/daemon.json

    Y se debe añadir el siguiente contenido:

    .. code-block:: json

        {
            "log-driver": "json-file",
            "log-opts": {
                "max-size": "10m",
                "max-file": "3"
            }
        }

NeoVim
______

NeoVim es un potente editor de terminal. Con un par de plugins, se puede transformar en un IDE completo.

Instalación
~~~~~~~~~~~

.. code-block:: bash

    sudo pacman -S neovim fd lazygit ripgrep python-pynvim --needed

Configuración
~~~~~~~~~~~~~

La configuración utilizada es `https://nvchad.com <https://nvchad.com>`_.

Qemu/KVM + VirtManager
______________________

QEMU es un emulador de procesadores basado en la traducción dinámica de binarios, es decir, realiza la conversión del código binario de la arquitectura fuente o host, en código entendible por la arquitectura huésped o la VM.

KVM es el módulo del kernel de Linux que permite esta asignación de CPU físico para CPU virtual. Esta asignación proporciona la aceleración de hardware para la máquina virtual y aumenta su rendimiento. De hecho QEMU utiliza esta aceleración cuando el tipo de virtualización es elegido como KVM.

Instalación
~~~~~~~~~~~

La mejor opción para virtualizar en Linux. Lo primero que hacemos es instalar los paquetes necesarios:

.. code-block:: bash

    sudo pacman -S archlinux-keyring qemu-desktop samba vde2 dmidecode ebtables dnsmasq libvirt bridge-utils openbsd-netcat radvd virt-manager virt-viewer ifplugd tcl edk2-ovmf libguestfs

.. warning::
    Nos preguntará si queremos reemplazar ``iptables`` por ``iptables-nft``, le diremos que **si** para poder instalar ebtables, el cual es necesario para poder tener conectividad de red en las máquinas virtuales.

Habilite la cuenta de usuario normal para usar KVM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dado que queremos usar nuestra cuenta de usuario estándar de Linux para administrar KVM, configuremos KVM para permitir esto:

.. code-block:: bash

    sudo nano /etc/libvirt/libvirtd.conf

Establezca la propiedad del grupo de sockets de dominio UNIX en libvirt:

.. code-block:: text

    unix_sock_group = "libvirt"

Establezca los permisos de socket UNIX para el socket R/W:

.. code-block:: text

    unix_sock_rw_perms = "0770"

Agregamos nuestro usuario a los grupos kvm y libvirt:

.. code-block:: bash

    sudo usermod -aG kvm $USER
    sudo usermod -aG libvirt $USER

Cargamos los módulos necesarios. En caso de tener un procesador intel sería así:

.. code-block:: bash

    sudo modprobe kvm-intel
    sudo modprobe kvm

En caso de ser AMD sería así:

.. code-block:: bash

    sudo modprobe kvm-amd
    sudo modprobe kvm

Ahora habilitamos el servicio:

.. code-block:: bash

    sudo systemctl enable libvirtd
    sudo systemctl start libvirtd

Ahora reinicie y puede utilizar el gestor.


Flutter
_______

Instalación de Flutter
~~~~~~~~~~~~~~~~~~~~~~

Para la instalación de Flutter es recomendable que lea la `guía oficial. <https://docs.flutter.dev/get-started/install/linux>`_

Establecer variables de entorno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para que ``Flutter`` pueda reconocer la instalación de los paquetes debe añadir al ``PATH`` y crear las variables de entorno. Para eso, yo tengo un archivo `.baserc` que se ejecuta dentro de la configuración de inicio de mis terminales:

.. code-block:: bash

    # Flutter
    if [ -d "/opt/flutter/bin" ]; then
        export PATH="/opt/flutter/bin:$PATH"
    fi

    export CHROME_EXECUTABLE=/usr/bin/google-chrome-stable
    export ANDROID_SDK_ROOT=/opt/android-sdk

Instalación de Java
~~~~~~~~~~~~~~~~~~~

Para instalar la última versión de Java solo debe ejecutar:

.. code-block:: bash

    sudo pacman -S jdk-openjdk

.. note::

    Una vez terminada la instalación debe reiniciar el equipo o ejecutar:

    .. code-block:: bash

        source /etc/profile.d/jre.sh

Instalación de SDK Manager para Android
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debe descargar el `command line tools` desde la `página de Android SDK. <https://developer.android.com/studio/index.html#command-line-tools-only>`_

Luego creo la carpeta ``/opt/android-sdk`` y ejecuto:

.. code-block:: bash

    ./sdkmanager --sdk_root=$ANDROID_SDK_ROOT --install "cmdline-tools;latest" "build-tools;33.0.1" "platform-tools" "platforms;android-29" "sources;android-29" "system-images;android-29;google_apis_playstore;x86_64"

Comprobación de la instalación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debe hacer referencia a la carpeta donde instaló su ``sdk-manager``:

.. code-block:: bash

    flutter config --android-sdk $ANDROID_SDK_ROOT

Aceptar los términos de licencia del SDK de Android:

.. code-block:: bash

    flutter doctor --android-licenses

Si todo lo anterior salió bien, al ejecutar el siguiente comando todo lo que usted espera que este correcto debe figurar con verde:

.. code-block:: bash

    flutter doctor -v

Obteniendo una salida similar a la siguiente:

.. code-block:: bash

    > flutter doctor
    Doctor summary (to see all details, run flutter doctor -v):
    [✓] Flutter (Channel stable, 3.3.10, on Arch Linux 6.0.12-arch1-1, locale es_AR.UTF-8)
    [✓] Android toolchain - develop for Android devices (Android SDK version 33.0.1)
    [✓] Chrome - develop for the web
    [✓] Linux toolchain - develop for Linux desktop
    [!] Android Studio (not installed)
    [✓] Connected device (2 available)
    [✓] HTTP Host Availability

    ! Doctor found issues in 1 category.

Y puede ver los dispositivos listos para desarrollar con:

.. code-block:: bash

    flutter devices

Qt 6
____

Para instalar Qt, simplemente ejecute:

.. code-block:: bash

    sudo pacman -S qt6 pyside6 qtcreator
