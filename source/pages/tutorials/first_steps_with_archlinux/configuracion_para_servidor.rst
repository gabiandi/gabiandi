##############################
Configuración para el servidor
##############################

.. contents:: Contenido
    :depth: 2
    :local:

Configuración base
==================

Establecer IP estática
______________________

Si disponemos o damos algún servicio, seguramente necesitaremos dejar una IP fija para que el servidor sea mas facil de ubicar en la red, esto lo podemos hacer con Network Manager.

Primero listamos las conexiones que tenemos:

.. code-block:: bash

    nmcli connection

Vamos a utilizar el nombre de la conexión para setear los parametros en el *nmcli*:

.. code-block:: bash

    nmcli connection modify "Conexión cableada 1" \
    ipv4.addresses "192.168.1.200/24" \
    ipv4.gateway "192.168.1.1" \
    ipv4.dns "192.168.1.1,8.8.8.8" \
    ipv4.method "manual"

Todos los archivos de configuración se guardan en ``/etc/NetworkManager/system-connections``.

Wake On Lan
___________

Podemos configurar nuestra placa de RED y el sistema para poder despertar via LAN.

La placa base de la computadora de destino y el controlador de interfaz de red deben admitir Wake-on-LAN. La computadora de destino debe estar conectada físicamente (con un cable) a un enrutador o a la computadora de origen para que WOL funcione correctamente. Algunas tarjetas inalámbricas son compatibles con Wake on Wireless (WoWLAN o WoW).

Para averiguar el estado de la interfaz de red debemos instalar *ethtool*:

.. code-block:: bash

    sudo pacman -S ethtool

Cuando la herramienta este instalada podemos comprobar el estado de alguna placa de red con el comando:

.. code-block:: bash

    sudo ethtool nombre_interfaz

Casi al final de la descripcion habrá una linea que dice:

.. code-block:: text

    Wake-on: d

Esta latra que le sigue a al campo dice en que estado esta la placa de red:

- d (disabled)
- p (PHY activity)
- u (unicast activity)
- m (multicast activity)
- b (broadcast activity)
- a (ARP activity)
- g (magic packet activity)

Para WOL se requiere que este en modo g. Podemos cambiar el estado de la placa con:

.. code-block:: bash

    sudo ethtool -s nombre_interfaz wol g

Ahora si comprobamos el estado nuevamente vemos que cambio. Pero esto puede restablecerse en el proximo reinicio. Si lo hace podes hacer lo siguiente para dejar permanente la configuración.

Creamos el archivo ``/etc/systemd/system/wol@.service`` con lo siguiente:

.. code-block:: text

    [Unit]
    Description=Wake-on-LAN for %i
    Requires=network.target
    After=network.target

    [Service]
    ExecStart=/usr/bin/ethtool -s %i wol g
    Type=oneshot

    [Install]
    WantedBy=multi-user.target

Entonces podemos habilitar el servicio con:

.. code-block:: bash

    sudo systemctl enable wol@nombre_interfaz

Y listo ya podemos hacer Wake On LAN.

Crear unidades RAID
___________________

Si se esta montando un servidor de archivos y tenes varios discos, lo mas seguro es que un esquema RAID se adapte a las necesidades de un file server.

Para la gestión de un RAID vamos a utilizar *mdadm*:

.. code-block:: bash

    sudo pacman -S mdadm

Para preparar nuestros discos debemos crear las particiones con la utilidad *gdisk* o *fdisk*, como los discos de mas de 2TB se llevan bien con GPT vamos a utilizar *gdisk*. Creamos las particiones y les damos un tipo Linux RAID con código FD00.

Cuando tengamos los discos listos, vamos a crear la unidad RAID, en mi caso creo un RAID5 con 4 discos:

.. code-block:: bash

    sudo mdadm --create --verbose --level=5 --chunk=512 --raid-devices=4 /dev/md/datos /dev/sdb1 /dev/sdc1 /dev/sdd1 /dev/sde1

Cuando el proceso inicie, la sincronización demorará un tiempo largo, podemos ver el proceso con:

.. code-block:: bash

    cat /proc/mdstat

Una vez el proceso termine, debemos establecer la configuración para que el RAID este configurado con cada inicio del sistema, para eso ejecutamos el comando:

.. code-block:: bash

    sudo mdadm --detail --scan

Y con la salida que nos da este comando editamos el archivo */etc/mdadm/mdadm.conf*:

.. code-block:: bash

    sudo nano /etc/mdadm.conf

Al final del archivo añadimos lo obtenido por el comando anterior:

.. code-block:: text

    ARRAY /dev/md/datos metadata=1.2 spares=1 name=gabi-sv:datos UUID=4d255030:3d4ac77d:3fa4f00d:7008e316

Le damos un formato a nuestro RAID:

.. code-block:: bash

    sudo mkfs.ext4 -v -L datos -b 4096 -E stride=128,stripe-width=384 /dev/md/datos

Creamos una carpeta y montamos el sistema:

.. code-block:: bash

    sudo mkdir /mnt/datos
    sudo mount /dev/md/datos /mnt/datos

Actualizamos en fstab para que el disco se monte al iniciar el sistema. Abrimos el archivo y añadimos al final:

.. code-block:: text

    UUID=d50ae083-8ab7-47ba-82dc-fafe85ded302 /mnt/datos ext4 rw,relatime 0 1

Podemos obtener el UUID de nuestro RAID con el comando:

.. code-block:: bash

    sudo blkid

A partir de ahora cuando el sistema se inicie el RAID se montará automáticamente.

Y listo tenemos configurado el dispositivo para poder transferir datos a el. Si queremos escribir contenido sin ser sudo debemos cambiar los permisos.

Servicios
=========

Servidor SSH
____________

Primero instalamos el servidor:

.. code-block:: bash

    sudo pacman -S openssh

Luego habilitamos el servicio:

.. code-block:: bash

    sudo systemctl enable sshd

Generar claves SSH
~~~~~~~~~~~~~~~~~~

Para generar un conjunto de llaves SSH ejecutamos:

.. code-block:: bash

    ssh-keygen -t rsa -b 4096 -C gabi -f ~/.ssh/id_rsa_gabi-sv
    ssh-copy-id -i ~/.ssh/id_rsa_gabi-sv gabi@192.168.1.200

Servidor de archivos SAMBA
__________________________

Samba es el conjunto de programas de interoperabilidad estándar de Windows para Linux y Unix. Desde 1992, Samba ha proporcionado servicios de impresión y archivos seguros, estables y rápidos para todos los clientes que utilizan el protocolo SMB/CIFS, como todas las versiones de DOS y Windows, OS/2, Linux y muchos otros.

Para configurar Samba se tiene que instalar el paquete:

.. code-block:: bash

    sudo pacman -S samba

Samba se configura en ``/etc/samba/smb.conf``, que está ampliamente documentado en la wiki de arch. Es necesario crear este archivo antes de iniciar el servicio, para ello se puede tener una plantilla `del repositorio de Samba en GitHub. <https://git.samba.org/samba.git/?p=samba.git;a=blob_plain;f=examples/smb.conf.default;hb=HEAD>`_

Luego de crear el archivo habilitamos los servicios:

.. code-block:: bash

    sudo systemctl enable smb
    sudo systemctl enable nmb
    sudo systemctl enable avahi-daemon

Podemos también iniciarlos:

.. code-block:: bash

    sudo systemctl start smb
    sudo systemctl start nmb
    sudo systemctl start avahi-daemon

Para agregar usuarios a samba estos deben ser usuarios de linux, para añadir usuarios a linux podemos hacerlo con:

.. code-block:: bash

    sudo useradd nombre_usuario

Luego creamos el usuario en samba:

.. code-block:: bash

    sudo smbpasswd -a nombre_usuario

Podemos listar los usarios con:

.. code-block:: bash

    sudo pdbedit -L

Podemos crear también grupos:

.. code-block:: bash

    sudo groupadd grupo

Para grupos de samba:

.. code-block:: bash

    sudo gpasswd grupo -a nombre_usuario

