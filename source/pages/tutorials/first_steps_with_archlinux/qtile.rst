####################
Instalación de QTile
####################

.. contents:: Contenido
    :depth: 2
    :local:

Carpetas de usuario básicas
===========================

Si quieres tener en tu carpeta de usuario, carpetas como *Documentos*, *Descargas*, *Música*, *etc*. Instalamos el servicios de carpetas de usuario:

.. code-block:: bash

    sudo pacman -S xdg-user-dirs

Y corremos la utilidad:

.. code-block:: bash

    xdg-user-dirs-update

Instalación de programas previos
================================

Antes de instalar Qtile, es necesario que contemos con un par de utilidades y fuentes:

.. code-block:: bash

    sudo pacman -S alacritty rofi ttf-dejavu ttf-liberation noto-fonts playerctl arandr udiskie network-manager-applet volumeicon libnotify notification-daemon xorg-xinit libmtp pamixer pipewire-alsa pipewire-pulse pipewire-audio pavucontrol

.. note::

    Si tenes una notebook, seguramente quieras alguna utilidad para la batería o el backlight:

    .. code-block:: bash

        sudo pacman -S brightnessctl cbatticon

Instalamos un *init session manager* como ``lightdm``:

.. code-block:: bash

    sudo pacman -S lightdm lightdm-webkit2-greeter
    git clone https://aur.archlinux.org/lightdm-webkit-theme-aether.git
    cd lightdm-webkit-theme-aether
    makepkg -si

Habilitamos el inicio de sesión en modo gráfico:

.. code-block:: bash

    sudo systemctl enable lightdm

.. note::

    Si no quiere instalar un *init session manager* deberá, configurar ``startx`` junto con ``xinit``:

    .. code-block:: bash

        sudo cp /etc/X11/xinit/xinitrc ~/.xinitrc

    Ahora sobre el final tiene que borrar:

    .. code-block:: conf

        twm &
        xclock -geometry 50x50-1+1 &
        xterm -geometry 80x50+494+51 &
        xterm -geometry 80x20+494-0 &
        exec xterm -geometry 80x66+0+0 -name login

    Y añadir:

    .. code-block:: conf

        qtile start

    Ya puede iniciar con:

    .. code-block:: bash

        startx

Habilitamos el servicio para las notificaciones:

.. code-block:: bash

    sudo nano /usr/share/dbus-1/services/org.freedesktop.Notifications.service

Y copiamos el siguiente contenido:

.. code-block:: text

    [D-BUS Service]
    Name=org.freedesktop.Notifications
    Exec=/usr/lib/notification-daemon-1.0/notification-daemon

Instalación de Qtile
====================

Ahora podemos proceder a instalar Qtile:

.. code-block:: bash

    sudo pacman -S qtile

Esto es todo para tener el gestor de ventanas funcionando, sin embargo, necesitamos unas utilidades básicas para poder movernos bien por el sistema, como ejemplo un gestor de archivos gráfico como ``thunar`` y compositor como ``picom``:

.. code-block:: bash

    sudo pacman -S thunar tumbler poppler-glib ffmpegthumbnailer gvfs gvfs-smb picom lxappearance scrot python-pip python-setuptools exa bat pass gnome-keyring xclip

Bluetooth
=========

Para instalar y habilitar bluetooth:

.. code-block:: bash

    sudo pacman -S blueman

Y para habilitar el servicio junto con el applet:

.. code-block:: bash

    sudo systemctl enable bluetooth
    sudo systemctl start bluetooth


Mis configuraciones
===================

A continuación, les dejo un link a mi `repositorio con mis dotfiles. <https://github.com/GabiAndi/dotfiles>`_
