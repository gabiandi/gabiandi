#############################
Primeros pasos con Arch Linux
#############################

.. contents:: Contenido
    :depth: 2
    :local:

Sobre Arch Linux
================

Arch Linux es una distribución Linux para computadoras x86\_64 orientada a usuarios avanzados. Se compone en su mayor parte de software libre y de código abierto. Su modelo de desarrollo es de tipo `rolling release <https://es.wikipedia.org/wiki/Liberaci%C3%B3n_continua>`_ y el enfoque de diseño persigue el `principio KISS. <https://es.wikipedia.org/wiki/Principio_KISS>`_

Para instalar y configurar este sistema operativo se necesita un grado de conocimiento superior al básico. No obstante, se puede mantener y administrar el sistema de forma sencilla. Los creadores y la comunidad, denominan como *filosofía*, los siguientes tres aspectos:

#. Mantener el sistema lo más simple y ligero posible, siguiendo el llamado `principio KISS. <https://es.wikipedia.org/wiki/Principio_KISS>`_
#. Los principios del liderazgo del proyecto, Aaron Griffin, también son tomados como referencia: *Fiarse de las GUIs para construir y configurar tu sistema operativo termina dañando a los usuarios finales. Intentar ocultar la complejidad del sistema, termina complicando al sistema. Las capas de abstracción que sirven para ocultar el funcionamiento interno nunca son una cosa buena. En cambio, los componentes internos deberían ser diseñados de forma que no necesiten ser ocultados*.
#. Arch Linux permite al usuario hacer las contribuciones que desee, mientras estas no vayan en contra de la filosofía.

Instalación
===========

.. note::
    Este documento es un resumen de instalación sacado de la `guía oficial de Arch Linux <https://wiki.archlinux.org/title/Installation_guide>`_, en donde se detalla en profundidad el proceso de instalación. Por lo que recomendamos que lea la documentación oficial antes de seguir con este post.

Obtener la ISO
______________

Podemos obtener la última instantánea de la `página de Arch Linux <https://archlinux.org/>`_ y grabarla en un USB con mínimo 4G de espacio.

Luego de verificar que la ISO se descargo correctamente, podemos pasarla al USB con el comando ``dd``:

.. code-block:: bash

    sudo dd if=ruta_de_la_iso of=dispositivo status=progress
    sync

Usamos sync para asegurarnos de que todo el cache se guardo en el dispositivo.

Iniciar desde el USB
____________________

Debemos bootear desde el USB para poder iniciar el proceso de instalación. Normalmente, cuando se inicia el Arch Live preguntará con que opción queremos bootear. Luego de bootear el sistema inicia y nos mostrará una consola completamente vacía a la espera de comandos.

Configuración de teclado
________________________

Es necesario primero que nada configurar la distribución del teclado para tener correctas las teclas. En el caso de un teclado *Español Latinoamerica*, el comando sería el siguiente:

.. code-block:: bash

    loadkeys la-latin1

Conectar a internet
___________________

La instalación del sistema requiere conexión a internet para poder descargar todos los paquetes necesarios. Para verificar la conexión a internet ejecutamos:

.. code-block:: bash

    ip a

Si estamos conectados por Ethernet (que es lo mas recomendable), la conexión se debería haber realizado automáticamente. De modo contrario se puede conectar a una red WiFi usando una utilidad de la instalación:

.. code-block:: bash

    iwctl

Cuando ingrese este comando se activará la utilidad, y podrá ver los dispositivos wireless disponibles en el equipo:

.. code-block:: bash

    device list

Puede escanear las redes disponibles:

.. code-block:: bash

    station wlan0 scan

Ahora espere unos 10 segundos a que termine el escaneo, luego podrá ver las redes disponibles:

.. code-block:: bash

    station wlan0 get-networks

Para conectarse a una red puede correr:

.. code-block:: bash

    station wlan0 connect nombre_del_wifi

Se le pedirá la contraseña, y se intentará conectar a la red indicada. Luego puede salir de la utilidad y volver a comprobar si tiene acceso a la red:

.. code-block:: bash

    exit

Actualizar el reloj del sistema
_______________________________

.. code-block:: bash

    timedatectl set-ntp true

Particionar el disco
____________________

Lo que se debe hacer ahora es crear las particiones de disco en donde se instalará el sistema. Podemos ver la cantidad de discos y sus particiones ejecutando:

.. code-block:: bash

    lsblk

La utilidad recomendada para crear particiones en sistemas GPT es ``gdisk`` y para MBR es ``fdisk``:

.. code-block:: bash

    gdisk /dev/sdx

    fdisk /dev/sdx

Para poder instalar cualquier distribución Linux en un Sistema EFI, debemos crear obligatoriamente las siguientes particiones:

#. Partición EFI con formato FAT32 de al menos 500MB.
#. Partición raíz con formato ext4.

Adicionalmente, es recomendable crear una partición de intercambio SWAP. Y también se pueden crear particiones extra para el ``/home`` o ``/opt``.

Estas particiones adicionales se deben formatear y montar en la ubicación correspondiente.

.. warning::
    Si su sistema **no es UEFI**, no deberá crear la partición EFI, todo lo demás aplica para ambos sistemas.

Formatear las particiones creadas
_________________________________

Una vez creamos las particiones que usaremos, procedemos a darles el formato correspondiente.

Para particiones ext4:

.. code-block:: bash

    mkfs.ext4 /dev/sdxn

Para particiones fat32:

.. code-block:: bash

    mkfs.fat -F32 /dev/sdxn

Para particiones swap:

.. code-block:: bash

    mkswap /dev/sdxn

Montar las particiones creadas
______________________________

Este es el paso previo a instalar el sistema, debemos montar las unidades configuradas anteriormente en las ubicaciones correspondientes, para luego generar el archivo de montajes al inicio del sistema. Para este tutorial, el sistema se montara en ``/mnt`` del Live USB.

La partición del sistema raíz:

.. code-block:: bash

    mount /dev/sdxn /mnt

Para la partición UEFI, debemos crear una ruta especial bajo la raíz llamada */boot*:

.. code-block:: bash

    mkdir /mnt/boot
    mount /dev/sdxn /mnt/boot

Particiones extras como */home* o */opt* también deben estar dentro de la raíz:

.. code-block:: bash

    mkdir /mnt/home
    mount /dev/sdxn /mnt/home

    mkdir /mnt/opt
    mount /dev/sdxn /mnt/opt

Para la partición SWAP:

.. code-block:: bash

    swapon /dev/sdxn

Instalación de Arch Linux
_________________________

Una vez tenemos todos los pasos anteriores cumplidos pasamos a finalmente instalar el sistema en las particiones que elegimos. Con *pacstrap* seleccionamos la ubicación de instalación (en mi caso ``/mnt``) y elegimos los paquetes a instalar:

.. code-block:: bash

    pacstrap /mnt base base-devel git linux linux-firmware linux-firmware-qlogic linux-headers nano networkmanager dialog bash-completion

Generar el archivo fstab
________________________

Para que las particiones se monten al arranque y cumplan con su función debemos generar el *fstab*:

.. code-block:: bash

    genfstab -U /mnt >> /mnt/etc/fstab

Cambiamos a la raíz de la instalación
_____________________________________

Para utilizar el sistema recién instalado, podemos utilizar el siguiente comando:

.. code-block:: bash

    arch-chroot /mnt

Configuramos la zona horaria
____________________________

.. code-block:: bash

    ln -sf /usr/share/zoneinfo/America/Argentina/Buenos_aires /etc/localtime

Sincronizamos el reloj
______________________

.. code-block:: bash

    hwclock --systohc

Establecemos el idioma
______________________

Lo primero que debemos hacer es seleccionar el idioma que utilizará el sistema:

.. code-block:: bash

    nano /etc/locale.conf

Y añadimos la siguiente linea:

.. code-block:: text

    LANG=es_AR.UTF-8

Paso siguiente configuramos el archivo de locales. Debemos descomentar nuestro idioma preferido y también uno en ingles que será el base:

.. code-block:: bash

    nano /etc/locale.gen

Y descomentamos:

.. code-block:: text

    en_US.UTF-8 UTF-8
    es_AR.UTF-8 UTF-8

Distribución de teclado
_______________________

Ahora creamos un archivo para la distribución del teclado:

.. code-block:: bash

    nano /etc/vconsole.conf

Y añadimos:

.. code-block:: text

    KEYMAP=la-latin1
    FONT=lat9-16

Generamos el archivo de locales
_______________________________

Generamos los archivos de configuración para establecer los cambios que generamos anteriormente:

.. code-block:: bash

    locale-gen

Configuración de la red
_______________________

Debemos darle un nombre a nuestra PC, para que sea identificable para otros equipos, lo podemos hacer editando el archivo:

.. code-block:: bash

    nano /etc/hostname

Y añadimos:

.. code-block:: text

    myhostname

Lo mismo para el archivo de hosts, este configurará otras cuestiones para el *NetworkManager*:

.. code-block:: bash

    nano /etc/hosts

Y añadimos lo siguiente:

.. code-block:: text

    127.0.0.1   localhost
    ::1         localhost
    127.0.1.1   myhostname.localdomain myhostname

Se habilita el NetworkManger
____________________________

Debemos habilitar el servicio de administración de redes, yo personalmente prefiero NetworkManger:

.. code-block:: bash

    systemctl enable NetworkManager

Cambio de contraseña de root
____________________________

Ingrese la contraseña de usuario ``root`` que prefiera, esto podemos eliminarlo mas tarde por seguridad, en un inicio vamos a logearnos en nuestro sistema como ``root``:

.. code-block:: bash

    passwd

Instalación del bootloader
__________________________

A continuación se instalara GRUB para UEFI o BIOS, esto permitirá al sistema arrancar:

.. code-block:: bash

    pacman -S grub ntfs-3g

.. note::
    Si tenes un sistema UEFI adicional debes instalar:

    .. code-block:: bash

        pacman -S efibootmgr

Es recomendable también instalar los microcodes, según la versión del procesador.

Para **AMD**:

.. code-block:: bash

    pacman -S amd-ucode

Para **INTEL**:

.. code-block:: bash

    pacman -S intel-ucode

.. note::
    Para sistemas **UEFI** configure el grub para su sistema bajo la ruta de la partición **EFI**:

    .. code-block:: bash

        grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id='arch'

.. note::
    Para sistemas **BIOS** debemos instalar el grub en la partición **MBR** del disco de arranque directamente:

    .. code-block:: bash

        grub-install --target=i386-pc /dev/sdx

.. note::

    Si tenes otro sistema operativo en alguna otra unidad o partición y queres que grub la detecte, instale el siguiente paquete:

    .. code-block:: bash

        pacman -S os-prober

    Para habilitar la detección automática se debe de editar el archivo ``/etc/default/grub``, y descomentar la sección:

    .. code-block:: text

        #GRUB_DISABLE_OS_PROBER=false

Ahora generamos la configuración básica del grub:

.. code-block:: bash

    grub-mkconfig -o /boot/grub/grub.cfg

.. note::
    Si tu sistema es **UEFI** se debe copiar el archivo de inicio:

    .. code-block:: bash

        mkdir /boot/EFI/boot
        cp /boot/EFI/arch/grubx64.efi /boot/EFI/boot/bootx64.efi

Finalizamos la instalación
__________________________

Salimos de arch-chroot:

.. code-block:: bash

    exit

Desmontamos todas las unidades de la instalación:

.. code-block:: bash

    umount -R /mnt

Y por último apagamos el equipo:

.. code-block:: bash

    systemctl poweroff

Ahora desconectamos el medio de instalación y ya podemos volver a iniciar el sistema.

Primer inicio de sesión
=======================

Conectar a una red WiFi
_______________________

Usando *NetworkManager* podemos conectarnos a una red:

.. code-block:: bash

    nmcli dev wifi connect nombre_de_wifi password contraseña

Creamos un usuario
__________________

Una nueva instalación te deja solo con la cuenta de superusuario, más conocida como ``root``. Iniciar sesión como ``root`` durante períodos prolongados de tiempo, posiblemente incluso exponerlo a través de SSH en un servidor, es inseguro. Es por eso que debemos crear un usuario para nosotros:

.. code-block:: bash

    useradd -m -G ftp,http,log,rfkill,sys,uucp,audio,storage,video,wheel,games,power,scanner,kvm -s /bin/bash nombre_usuario

Creamos una contraseña para el nuevo usuario
____________________________________________

Para mayor seguridad es recomendable que le pongamos contraseña al nuevo usuario creado:

.. code-block:: bash

    passwd nombre_usuario

Añadimos a sudo el nuevo usuario
________________________________

Editamos el archivo de sudo y luego descomentamos la linea que habilita al grupo wheel:

.. code-block:: bash

    EDITOR=nano visudo

Y descomentamos la linea:

.. code-block:: text

    %wheel ALL=(ALL) ALL

Esto permitirá que usuarios que pertenezcan al grupo wheel puedan usar ``sudo``.

Iniciamos sesión con el nuevo usuario
_____________________________________

Salimos de la sesión de root:

.. code-block:: bash

    exit

Y nos logeamos con el nuevo usuario y contraseña.

Actualizamos el sistema
_______________________

Ahora que tenemos el sistema instalado y configurado con una cuenta de usuario válida, lo que debemos hacer es comprobar si existen actualizaciones disponibles. Pero antes, es buena idea habilitar el soporte multilib y unas opciones más:

.. code-block:: bash

    sudo nano /etc/pacman.conf

Y descomentamos las siguientes lineas:

.. code-block:: text

    Color
    ParallelDownloads = 6

    [multilib]
    Include = /etc/pacman.d/mirrorlist

Luego, actualizamos la lista de repositorios y verificamos las actualizaciones:

.. code-block:: bash

    sudo pacman -Syu

Drivers base
____________

Si ejecutamos:

.. code-block:: bash

    sudo mkinitcpio -p linux

Y obtenemos una salida como la siguiente:

.. code-block:: text

    ==> WARNING: Possibly missing firmware for module: aic94xx
    ==> WARNING: Possibly missing firmware for module: wd719x
    ==> WARNING: Possibly missing firmware for module: xhci_pci
    ==> WARNING: Possibly missing firmware for module: ast

Tenemos un problema con drivers que nos estan faltando. La solucion es instalarlos para tener el mejor rendimiento de nuestro hardware. Para ello nos dirigimos a una carpeta temporal en donde descargar archivos:

.. code-block:: bash

    cd

Y ahora luego de buscar los drivers en AUR o el repositorio oficial, los instalamos:

.. code-block:: bash

    git clone https://aur.archlinux.org/aic94xx-firmware.git
    cd aic94xx-firmware
    makepkg -si

    git clone https://aur.archlinux.org/wd719x-firmware.git
    cd wd719x-firmware
    makepkg -si

    git clone https://aur.archlinux.org/ast-firmware.git
    cd ast-firmware
    makepkg -si

    git clone https://aur.archlinux.org/upd72020x-fw.git
    cd upd72020x-fw
    makepkg -si

Cuando se instalen los drivers ahora podemos borrar los directorios de compilación.

Configuración de un entorno de escritorio
=========================================

Un entorno de escritorio es recomendado si la computadora en donde se esta instalando el sistema requiere de capacidades gráficas. Existen una variedad bastante grande de entornos de escritorios, a mi en lo personal me gustan dos: GNOME y KDE Plasma. Pero también puede instalar gestores tipo tile manager como QTile, Awesome, Xmonad, etc.

Antes de instalar un entorno debemos tener el servidor de gráficos. ``xorg`` esta en casi todo sistema, sin embargo, ``wayland`` esta ganando terreno últimamente.

Instalación de Xorg
___________________

Podemos instalar el servidor de ventanas X con el comando:

.. code-block:: bash

    sudo pacman -S xorg

Entornos tipo float window manager
__________________________________

.. toctree::
   :maxdepth: 1
   :includehidden:

   gnome
   kde_plasma

Entornos tipo tile window manager
_________________________________

.. toctree::
   :maxdepth: 1
   :includehidden:

   qtile

.. warning::

    Una vez instalado el entorno de escritorio deberá reiniciar el equipo.

Software base del sistema
=========================

Drivers de la tarjeta gráfica
_____________________________

Primero instalamos las utilidades de mesa, que son básicas a cualquier driver de tarjeta gráfica:

.. code-block:: bash

    sudo pacman -S mesa lib32-mesa mesa-demos mesa-utils --needed

.. note::

    Para instalar los drivers de **AMD** puede ejecutar los siguientes comandos:

    .. code-block:: bash

        sudo pacman -S xf86-video-amdgpu xf86-video-amdgpu vulkan-radeon virtualgl

    ``virtualgl`` debe seleccionarse como ``opencl-rusticl-mesa``.

.. note::

    Para instalar los drivers de **NVIDIA** puede ejecutar los siguientes comandos:

    .. code-block:: bash

        sudo pacman -S nvidia nvidia-utils lib32-nvidia-utils nvidia-settings virtualgl

    ``virtualgl`` debe seleccionarse como ``opencl-nvidia``.

Una vez instalado los drivers se debe reiniciar la PC antes de hacer cualquier otra cosa.

Instalación de Plymouth
_______________________

Plymouth es un proyecto de Fedora y ahora figura entre los recursos oficiales de `freedesktop.org <https://www.freedesktop.org>`_ que proporciona un proceso de arranque gráfico sin parpadeos. Se basa en la configuración del modo kernel (KMS) para configurar la resolución nativa de la pantalla lo antes posible, luego proporciona una pantalla de inicio llamativa que conduce hasta el administrador de inicio de sesión.

Para poder instalarlo tenemos que clonar un repositorio de aur:

.. code-block:: bash

    git clone https://aur.archlinux.org/plymouth-git.git
    cd plymouth-git
    makepkg -si

Agregue plymouth a la HOOKS en mkinitcpio.conf. Debe agregarse después de ``base udev`` para que funcione: 

.. code-block:: bash

    sudo nano /etc/mkinitcpio.conf

Quedando el archivo como:

.. code-block:: text

    HOOKS=(base udev plymouth ...)

Luego, añadir ``quiet loglevel=3 splash vt.global_cursor_default=0`` a los parámetros del núcleo. Para ``grub2`` quedaría:

.. code-block:: bash

    sudo nano /etc/default/grub

.. code-block:: text

    GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=3 splash vt.global_cursor_default=0"
    GRUB_CMDLINE_LINUX="splash"

Ahora puede seleccionar los temas editando el archivo:

.. code-block:: bash

    sudo nano /etc/plymouth/plymouthd.conf

Por ejemplo:

.. code-block:: text

    [Daemon]
    Theme=script
    ShowDelay=8
    DeviceTimeout=8

O si esta utilizando otro tema, por ejemplo **spinner**, si quiere el logo de Arch:

.. code-block:: bash

    sudo cp /usr/share/plymouth/arch-logo.png /usr/share/plymouth/themes/spinner/watermark.png

.. code-block:: text

    [Daemon]
    Theme=spinner
    ShowDelay=8
    DeviceTimeout=8

Y para centrar el tema al archivo:

.. code-block:: bash

    sudo nano /usr/share/plymouth/themes/spinner/spinner.plymouth

.. code-block:: text

    WatermarkHorizontalAlignment=.5
    WatermarkVerticalAlignment=.5

Cuando tenga todo configurado ejecute:

.. code-block:: bash

    sudo mkinitcpio -p linux
    sudo grub-mkconfig -o /boot/grub/grub.cfg

.. warning::

    Todavía no reinicie Arch si desea instalar **optimus-manager**.

Instalación de optimus-manager
______________________________

Este programa de Linux proporciona una solución para el cambio de GPU en laptops Optimus *(es decir, laptops con una configuración dual Nvidia/Intel o Nvidia/AMD)*.

Para usuarios de **GNOME** primero hay que instalar un gestor de sesiones parcheado:

.. code-block:: bash

    git clone https://aur.archlinux.org/gdm-prime.git
    cd gdm-prime

Si utiliza ``plymouth``, cuando clone el repositorio debe habilitar el flag para el soporte en el archivo *PKGBUILD*:

.. code-block:: bash

    nano PKGBUILD

Busque la linea:

.. code-block:: text

    -D plymouth=enabled

Luego ya puede instalar:

.. code-block:: bash

    makepkg -si

Una vez instalada la utilidad, editamos el archivo */etc/gdm/custom.conf*:

.. code-block:: bash

    sudo nano /etc/gdm/custom.conf

Y descomentamos la siguiente linea:

.. code-block:: text

    WaylandEnable=false

.. warning::

    Se tiene que comprobar que no exista el archivo */etc/X11/xorg.conf*, si es asi, hay que eliminarlo.

Entonoces para instalarlo ejecutamos:

.. code-block:: bash

    git clone https://aur.archlinux.org/optimus-manager.git
    cd optimus-manager
    makepkg -si

.. warning::

    Despues de instalar se tiene que **reiniciar el ordenador.**

Una vez reiniciado ahora podemos ejecutar los siguientes comandos para cambiar el modo de uso de la gráfica:

.. code-block:: bash

    optimus-manager --switch nvidia
    optimus-manager --switch integrated
    optimus-manager --switch hybrid

Icono de optimus mánager
________________________

El programa ``optimus-manager-qt`` proporciona un icono en la bandeja del sistema para cambiar fácilmente entre gráficas. También incluye una GUI para configurar opciones sin editar el archivo de configuración manualmente.

Antes de instalar este software, es necesario tener un complemento de iconos en el shell activado, de otra manera no se podrá utilizar por mas que este programa este cargado en memoria:

.. code-block:: bash

    git clone https://aur.archlinux.org/optimus-manager-qt.git
    cd optimus-manager-qt

Antes de instalar si estamos usando el escritorio de **KDE Plasma** tenemos que habilitar las opciones extendidas:

.. code-block:: bash

    nano PKGBUILD

Y ponemos a true la siguiente linea:

.. code-block:: text

    _with_plasma=true

Ahora si podemos instalar, si tu escritorio no es KDE pasma el *PKGBUILD* no se toca:

.. code-block:: bash

    makepkg -si

Probar el rendimiento del sistema
_________________________________

Los siguientes comandos proporcionan información útil sobre el sistema gráfico.

Informacion de la placa utilizada:

.. code-block:: bash

    glxinfo | grep -i vendor
    glxinfo | grep render
    glxinfo | grep direct
    glxinfo | grep OpenGL

Benchmarks:

.. code-block:: bash

    glxgears -info
    glxspheres64 -info

Programas y utilidades
======================

.. toctree::
    :includehidden:
    :maxdepth: 1

    complementos_y_programas
    configuracion_para_servidor
