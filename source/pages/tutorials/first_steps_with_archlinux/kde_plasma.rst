#########################
Instalación de KDE Plasma
#########################

Si quieres tener en tu carpeta de usuario, carpetas como *Documentos*, *Descargas*, *Música*, *etc*. Instalamos el servicios de carpetas de usuario:

.. code-block:: bash

    sudo pacman -S xdg-user-dirs

Y corremos la utilidad:

.. code-block:: bash

    xdg-user-dirs-update

Para instalar KDE Plasma ejecutamos el siguiente comando:

.. code-block:: bash

    sudo pacman -S plasma plasma-meta kde-applications kde-applications-meta packagekit-qt5

Es recomendable instalar temas tanto para Qt y Gtk, ademas de otros paquetes base:

.. code-block:: bash

    sudo pacman -S papirus-icon-theme arc-gtk-theme cups ufw

Por último habilitamos servicios del sistema:

.. code-block:: bash

    sudo systemctl enable sddm
    sudo systemctl enable cups
    sudo systemctl enable bluetooth

Ahora podemos reiniciar la PC para entrar en el modo gráfico.