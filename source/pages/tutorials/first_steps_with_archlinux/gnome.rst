####################
Instalación de GNOME
####################

Si GNOME es tu escritorio, lo podes instalar tan fácil como el siguiente comando:

.. code-block:: bash

    sudo pacman -S gnome gnome-extra

También, recomiendo instalar el paquete de iconos Papirus y la utilidad de impresoras CUPS:

.. code-block:: bash

    sudo pacman -S papirus-icon-theme cups ufw

Por último, debemos iniciar el servicio de administrador de sesiones y de impresión:

.. code-block:: bash

    sudo systemctl enable gdm
    sudo systemctl enable cups
    sudo systemctl enable bluetooth

Ahora podemos reiniciar la PC para entrar en el modo gráfico.