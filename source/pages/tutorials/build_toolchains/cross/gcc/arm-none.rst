#######################################
Construir Toolchain para ARM Bare Metal
#######################################

.. warning::
  *Este documento se revisó por ultima vez el* **13 de septiembre del 2023.** *Por lo que si esta viendo
  este artículo pasado un buen tiempo desde la publicación, seguro tenga que complementar la
  información presente aquí.*

.. contents:: Contenido
    :depth: 2
    :local:

Construyendo el Toolchain
=========================

Preparar el Host
----------------

Dependiendo de su sistema operativo es como deberá instalar los paquetes necesarios.
En caso de **Arch Linux**:

.. code-block:: bash

  sudo pacman -Syu
  sudo pacman -S --needed base-devel python doxygen git openssl unzip wget ncurses rsync texlive-most gperf autogen guile diffutils gmp isl expat clang llvm cmake ninja meson graphviz gtk2

Preparar la estructura de carpetas
----------------------------------

En las siguientes instrucciones, asumiré que estás realizando todos los pasos en una carpeta
separada, y que mantenes abierta la misma sesión de terminal hasta que todo esté hecho. En mi caso:

- La ruta de descarga de las fuentes se hará en: ``$HOME/build-toolchain/src``.
- La ruta de compilación de los binarios se hará en: ``$HOME/build-toolchain/build``.

Ruta de instalación se refiere a donde se guardaran los binarios compilados del Toolchain.
En este tutorial se busca crear un compilador GCC portable. Una vez terminemos, empaquetaremos
todo en un archivo ``cross-$ARCH-$EABI-gcc-X.X.X.tar.xz`` que será trasladable a cualquier
sistema (siempre y cuando se cumplan las dependencias propias del Toolchain).

Antes de eso, para poder hacer mas sencilla la escritura de comandos, y así evitar errores de tipeo,
voy a exportar las rutas y configuraciones como variables de bash:

.. code-block:: bash

  export N_CPUS="4"

  export BINUTILS_VERSION="2.41"
  export GCC_VERSION="13.2.0"
  export NEWLIB_VERSION="4.3.0.20230120"
  export GDB_VERSION="13.2"

  export TARGET="arm-none-eabi"
  export TARGET_OPTIONS="--with-arch=armv6zk --with-fpu=vfp --with-float=hard --with-mode=arm"
  export EXTRA_OPTIONS="--disable-multilib --enable-multiarch --enable-lto --disable-nls --with-gnu-as --with-gnu-ld --disable-shared --disable-threads"
  export GCC_LANGUAGES="c,c++"

  export INSTALL_DIR_PREFIX="cross-armv6zk-hardfp"

  export WORK_DIR="$HOME/build-toolchain"
  export SRC_DIR="$WORK_DIR/src"
  export BUILD_DIR="$WORK_DIR/build"

  export INSTALL_DIR="$BUILD_DIR/$INSTALL_DIR_PREFIX-gcc-$GCC_VERSION"

  export SAVE_TOOLCHAIN_DIR="$HOME"

Creamos las carpetas base en donde construiremos todo:

.. code-block:: bash

  mkdir -p "$SRC_DIR" "$BUILD_DIR" "$INSTALL_DIR"

Obtenemos las fuentes
---------------------

Descarguemos lo necesario para construir el compilador cruzado. Binutils, Newlib, GCC y GDB.

Para la biblioteca estándar en mi caso utilizaré Newlib, ya que se adapta mejor a sistemas embebidos
o bare metal:

.. code-block:: bash

  cd "$SRC_DIR"

  wget "https://ftpmirror.gnu.org/binutils/binutils-$BINUTILS_VERSION.tar.xz"
  wget "https://ftpmirror.gnu.org/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.xz"
  wget "ftp://sourceware.org/pub/newlib/newlib-$NEWLIB_VERSION.tar.gz"
  wget "https://ftpmirror.gnu.org/gnu/gdb/gdb-$GDB_VERSION.tar.xz"

  tar -xf "binutils-$BINUTILS_VERSION.tar.xz"
  tar -xf "gcc-$GCC_VERSION.tar.xz"
  tar -xf "newlib-$NEWLIB_VERSION.tar.gz"
  tar -xf "gdb-$GDB_VERSION.tar.xz"

  mkdir -p "$BUILD_DIR/binutils" "$BUILD_DIR/gcc" "$BUILD_DIR/newlib" "$BUILD_DIR/gdb"

Requisitos previos de GCC
-------------------------

GCC necesita algunos paquetes extras que debemos descargar dentro de la carpeta de origen:

.. code-block:: bash

  cd "$SRC_DIR/gcc-$GCC_VERSION"
  contrib/download_prerequisites

Ademas, para construir Newlib junto a GCC tenemos que crear una serie de enlaces simbólicos:

.. code-block:: bash

  ln -s "../newlib-$NEWLIB_VERSION/newlib" .
  ln -s "../newlib-$NEWLIB_VERSION/libgloss" .

Variables `PATH`
----------------

Durante todo el proceso de compilación, asegúrese de que el subdirectorio ``/bin`` de la instalación
esté en su ``PATH``. Puede eliminar este directorio del ``PATH`` luego de la instalación, pero la
mayoría de los pasos de compilación esperan encontrar ``arm-none-eabi-gcc`` y otras herramientas de
host a través del ``PATH``:

.. code-block:: bash

  export PATH="$INSTALL_DIR/bin:$PATH"

Construcción de Binutils
------------------------

Para poder utilizar nuestro compilador cruzado debemos incorporar Binutils a nuestra carpeta
resultante por ello es que descargamos las fuentes y procedemos a compilarlo.

A continuación, construimos Binutils para nuestro compilador GCC:

.. code-block:: bash

  cd "$BUILD_DIR/binutils"

  "$SRC_DIR/binutils-$BINUTILS_VERSION/configure" \
  --prefix= \
  --build="$MACHTYPE" \
  --host="$MACHTYPE" \
  --target="$TARGET" \
  "$TARGET_OPTIONS \
  $EXTRA_OPTIONS"

  make -j"$N_CPUS"
  make install-strip DESTDIR="$INSTALL_DIR"

Construcción de GCC con Newlib
------------------------------

Para Newlib es posible compilar GCC de manera directa, para hacer eso creamos los enlaces simbólicos
desde la carpeta de fuentes de Newlib hasta la de fuentes de GCC. Ahora solo hace falta construir:

.. code-block:: bash

  cd "$BUILD_DIR/gcc"

  "$SRC_DIR/gcc-$GCC_VERSION/configure" \
  --prefix= \
  --build="$MACHTYPE" \
  --host="$MACHTYPE" \
  --target="$TARGET" \
  "$TARGET_OPTIONS \
  $EXTRA_OPTIONS" \
  --with-newlib \
  --enable-languages="$GCC_LANGUAGES"

  make -j"$N_CPUS"
  make install DESTDIR="$INSTALL_DIR"

Construcción del depurador GDB
------------------------------

Lo siguiente que debemos hacer antes de probar el compilador es construir el depurador que estará
incluido en la lista de binarios:

.. code-block:: bash

  cd "$BUILD_DIR/gdb"

  "$SRC_DIR/gdb-$GDB_VERSION/configure" \
  --prefix= \
  --build="$MACHTYPE" \
  --host="$MACHTYPE" \
  --target="$TARGET" \
  "$TARGET_OPTIONS \
  $EXTRA_OPTIONS" \
  --enable-languages="$GCC_LANGUAGES" \
  --with-python="/usr/bin/python3"

  make -j"$N_CPUS"
  make install DESTDIR="$INSTALL_DIR"

Y listo, ahora tenemos el compilador cruzado completo.

Guardar el Toolchain recién construido
--------------------------------------

Si deseamos podemos comprimir y guardar en caso que querramos distribuirlos o hacer una copia de seguridad, para ello vaya a la carpeta donde instalo los compiladores:

.. code-block:: bash

  cd "$BUILD_DIR"
  tar -czf "$INSTALL_DIR_PREFIX-gcc-$GCC_VERSION.tar.xz" "$INSTALL_DIR_PREFIX-gcc-$GCC_VERSION"
  mv "$INSTALL_DIR_PREFIX-gcc-$GCC_VERSION.tar.xz" "$SAVE_TOOLCHAIN_DIR"

Conclusiones
============

Como se ve, en realidad esto no es una tarea demasiado complicada, hay que tener una guía sobre que
hacer (mas o menos), y después modificar los argumentos pasados a los scripts de compilación para
obtener tus herramientas personalizadas. En mi caso en particular tuve que aprender a hacerlo ya que
los compiladores oficiales de ARM GCC 6 o superior, vienen construidos para armv7 por defecto.
Esto hacia imposible que pueda usarlos en objetivos armv6zk como la Raspberry Pi 1.
