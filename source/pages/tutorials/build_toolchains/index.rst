################################
Construir cadena de herramientas
################################

Información del sistema de ejemplo
==================================

Para la creación de este tutorial se utiliza Arch Linux.

.. toctree::
   :caption: Tutoriales y guías
   :maxdepth: 1

   cross/gcc/arm-none
   qt/android
