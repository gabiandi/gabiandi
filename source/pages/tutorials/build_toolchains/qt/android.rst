######################################
Construir Qt para dispositivos Android
######################################

.. warning::
  *Este documento se revisó por ultima vez el* **28 de noviembre del 2023.** *Por lo que si esta
  viendo este artículo pasado un buen tiempo desde la publicación, seguro tenga que complementar la
  información presente aquí.*

.. contents:: Contenido
    :depth: 2
    :local:

Información
===========

Al momento de hacer esta guia la versión mas nueva de Qt es la 6.6.0. En mi caso utilizo un sistema
**Arch Linux x86_64**.

Construyendo el Toolchain
=========================

Preparar el Host
----------------

.. note::

  Esta instalación se realiza ya teniendo configurado un ``Qt creator`` con toda la instalación para
  escritorio.

Dependiendo de su sistema operativo es como deberá instalar los paquetes necesarios:

.. code-block:: bash

  sudo pacman -S --needed base-devel python doxygen git openssl unzip wget ncurses rsync texlive gperf autogen guile diffutils gmp isl expat clang llvm llvm-libs cmake ninja meson graphviz gtk2 jdk17-openjdk libc++

Ademas, debe de disponer de una instalación de ``android-sdk``.

Instalación de SDK Manager para Android
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debe descargar el `command line tools` desde la `página de Android SDK. <https://developer.android.com/studio/index.html#command-line-tools-only>`_

Luego creo la carpeta ``/opt/android-sdk`` y ejecuto:

.. code-block:: bash

    ./sdkmanager --sdk_root=$ANDROID_SDK_ROOT --install "cmdline-tools;latest" "build-tools;33.0.1" "platform-tools" "platforms;android-29" "sources;android-29" "system-images;android-29;google_apis_playstore;x86_64"

Para verificar que se tienen todos los requisitos, debe de inicar ``Qt creator`` y mirar en la
opción de Android si todo esta correctamente configurado.

Setear variables de entorno
---------------------------

A lo largo del tutorial necesitaremos unas variables de entorno que suponemos valen:

.. code-block:: bash

  export ANDROID_SDK_ROOT="/opt/android-sdk"
  export ANDROID_NDK_VERSION="25.1.8937393"
  export ANDROID_NDK_ROOT="$ANDROID_SDK_ROOT/ndk/$ANDROID_NDK_VERSION/"
  export QT_VERSION="6.6.0"
  export QT_HOST_INSTALLATION_PATH="/usr"
  export QT_ANDROID_BASE_PATH="$HOME/qt-android"
  export QT_ANDROID_PATH_TAR_XZ="$HOME/Descargas/qt-everywhere-src-$QT_VERSION.tar.xz"
  export QT_ANDROID_PATH="$QT_ANDROID_BASE_PATH/src/qt-everywhere-src-$QT_VERSION"
  export QT_ANDROID_INSTALL_BASE_PATH="/opt/qt"
  export QT_ANDROID_INSTALL_PATH="$QT_ANDROID_INSTALL_BASE_PATH/$QT_VERSION/android"

Descargar Qt
------------

Puede descargar el contenido del siguiente
`link <https://download.qt.io/official_releases/qt/6.6/6.6.0/single/>`_.
Comprobando la descarga con el hash ``efc59647689c2548961a7312d075baf6`` para
``qt-everywhere-src-$QT_VERSION.tar.xz``. En `esta página <https://doc.qt.io/qt-6/supported-platforms.html>`_
puede verificar si su plataforma esta soportada por Qt, pero de base, para IOs, Android y sistemas
de escritorio como Linux, Windows y MacOS funcionará y tendrá soporte oficial.

.. code-block:: bash

  md5sum qt-everywhere-src-$QT_VERSION.tar.xz && echo "efc59647689c2548961a7312d075baf6  qt-everywhere-src-$QT_VERSION.tar.xz"

Crear carpetas temporales de construcción
-----------------------------------------

Vamos a trabajar sobre una carpeta en el ``$HOME`` es por esto que creamos una estructura de
carpetas alli:

.. code-block:: bash

  mkdir "$QT_ANDROID_BASE_PATH"
  mkdir "$QT_ANDROID_BASE_PATH/src"
  mkdir "$QT_ANDROID_BASE_PATH/build"

Extraer el contenido del archivo
--------------------------------

Primero es necesario extraer el contenido del archivo comprimido:

.. code-block:: bash

  cd "$QT_ANDROID_BASE_PATH/src"
  cp "$QT_ANDROID_PATH_TAR_XZ" .
  tar xf "qt-everywhere-src-$QT_VERSION.tar.xz"

Configurar la contrucción de Qt
-------------------------------

Lo siguiente es crear una carpeta en donde se contruira Qt, y donde se instalará:

.. code-block:: bash

  mkdir "$QT_ANDROID_BASE_PATH/build/qt"
  cd "$QT_ANDROID_BASE_PATH/build/qt"
  sudo mkdir -p "$QT_ANDROID_INSTALL_PATH"
  sudo chown -R "$USER:$USER" "$QT_ANDROID_INSTALL_BASE_PATH"

Lo siguiente que debe de ejecutar es la configuración de la contrucción de Qt:

.. code-block:: bash

  "$QT_ANDROID_PATH/configure" \
  -platform android-clang \
  -prefix "$QT_ANDROID_INSTALL_PATH" \
  -android-ndk "$ANDROID_NDK_ROOT" \
  -android-sdk "$ANDROID_SDK_ROOT" \
  -qt-host-path "$QT_HOST_INSTALLATION_PATH"

Si no se emitio ningun error, verifique que se habilitaron las caracteristicas que usted desea.

Si quiere volver a configurar tendra que borrar todo en el directorio:

.. code-block:: bash

  rm -rf *

Si todo salio bien, ahora podrá construir los binarios de Qt. Este proceso demorará su tiempo:

.. code-block:: bash

  cmake --build . --parallel
  cmake --install .

Configuración de Qt Creator
===========================

Agregar la versión de Qt
------------------------

Vamos a la pestaña ``Qt versions`` y buscamos el ejecutable qmake generado.

Agregar Kit
-----------

En la pestaña ``Kits`` tenemos que agregar toda la configuración que hicimos anteriormente.
Verificando que no haya ningún inconveniente.
