########
Sobre mi
########

Mi nombre es Gabriel Aguirre, soy Ingeniero en Mecatrónica egresado de la `Universidad Nacional de Entre Ríos. <https://www.fcal.uner.edu.ar/academica/carreras/grado/ingenieria-en-mecatronica/>`_ Vivo en Concordia, Entre, Ríos, Argentina.

A lo largo de los años, la curiosidad me llevó a leer, investigar, realizar cursos, experimentar y probar siempre cosas nuevas. Es por esto que pude adquirir experiencia en el uso de distintos lenguajes de programación, entornos y herramientas.

Actualmente soy desarrollador de software en `Flexar SRL, <http://www.flexar.com.ar/>`_ en donde trabajo con tecnologías del lado del servidor como `Flask <https://flask.palletsprojects.com/>`_ o `Tryton, <https://www.tryton.org/>`_ y también del lado del cliente como `VueJs. <https://vuejs.org/>`_ Sin embargo, a lo largo de mi vida y mi carrera, siempre mostré una inclinación ante los sistemas embebidos y de bajo nivel, con lo cual e hecho muchos cursos y proyectos orientados a ello.

Habilidades
===========

Lenguajes de programación
_________________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/C_(lenguaje_de_programaci%C3%B3n)">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/c.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/C%2B%2B">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/cplusplus.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Java_(lenguaje_de_programaci%C3%B3n)">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/java.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Python">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/python.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Dart">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/dart.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/JavaScript">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/javascript.svg"/>
      </a>
   </p>

Python frameworks, IDEs, Tools, etc
___________________________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/TensorFlow">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/tensorflow.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Keras">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/keras.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/OpenCV">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/opencv.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Visual_Studio_Code">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/visualstudio.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Flask">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/flask.svg"/>
      </a>
   </p>

C/C++ frameworks, IDEs, Tools, etc
___________________________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/Qt_(biblioteca)">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/qt.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/OpenCV">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/opencv.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Visual_Studio_Code">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/visualstudio.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/CMake">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/cmake.svg"/>
      </a>
   </p>

Herramientas de desarrollo
__________________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/Docker">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/docker.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/GNU">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gnu.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Bash">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gnubash.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/VirtualBox">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/virtualbox.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/QEMU">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/qemu.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/VMware">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/vmware.svg"/>
      </a>
   </p>

Gestión de código
_________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/Git">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/git.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/GitHub">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/github.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/GitLab">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gitlab.svg"/>
      </a>
   </p>

Bases de datos
______________

.. raw:: html

   <p align="center">
      <a href="https://www.postgresql.org/">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/postgresql.svg"/>
      </a>
      <a href="https://www.mysql.com/">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/mysql.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Microsoft_SQL_Server">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/microsoftsqlserver.svg"/>
      </a>
   </p>

Sistemas operativos
___________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/Android">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/android.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Microsoft_Windows">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/windows.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/GNU/Linux">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/linux.svg"/>
      </a>
   </p>

Herramientas extra
__________________________

.. raw:: html

   <p align="center">
      <a href="https://es.wikipedia.org/wiki/Blender">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/blender.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Inkscape">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/inkscape.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/GIMP">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gimp.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Microsoft_Office">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/microsoftoffice.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/LibreOffice">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/libreoffice.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/LaTeX">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/latex.svg"/>
      </a>
      <a href="https://es.wikipedia.org/wiki/Markdown">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/markdown.svg"/>
      </a>
      <a href="https://readthedocs.org/">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/readthedocs.svg"/>
      </a>
   </p>

.. toctree::
   :caption: Proyectos
   :maxdepth: 1

   Módulo de Python para impresoras Toshiba <https://toshiba-printer-bsa4t.readthedocs.io/>

.. toctree::
   :caption: Tutoriales y guías
   :maxdepth: 2

   pages/tutorials/first_steps_with_archlinux/index
   pages/tutorials/mi_config_arch/index
   pages/tutorials/build_toolchains/index

Contacta conmigo
================

.. raw:: html

   <p align="center">
      <a href="https://www.linkedin.com/in/gabriel-andr%C3%A9s-aguirre-937297175">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/linkedin.svg"/>
      </a>
      <a href="https://www.instagram.com/gabiandresaguirre/">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/instagram.svg"/>
      </a>
      <a href="https://www.facebook.com/gabriel.aguirre2013">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/facebook.svg"/>
      </a>
      <a href="mailto:gabiandiagui@gmail.com">
         <img width="33px" src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gmail.svg"/>
      </a>
   </p>
